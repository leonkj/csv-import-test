<?php

declare(strict_types=1);

namespace Tests\FileImport\Enum;

use App\ImportProducts\Enum\Field;
use PHPUnit\Framework\TestCase;

class FieldTest extends TestCase
{
    private function provideTryFromCaseInsensitive(): array
    {
        return [
            ['Product Code', Field::ProductCode],
            ['product code', Field::ProductCode],
            ['Product Name', Field::ProductName],
            ['product name', Field::ProductName],
            ['Product Description', Field::ProductDescription],
            ['product description', Field::ProductDescription],
            ['Stock', Field::Stock],
            ['stock', Field::Stock],
            ['Cost In GBP', Field::CostInGBP],
            ['cost in gbp', Field::CostInGBP],
            ['Discontinued', Field::Discontinued],
            ['discontinued', Field::Discontinued],
            ['pROduct cOdE', Field::ProductCode],
            ['PRODUCT CODE', Field::ProductCode],
            ['something non existent', null],
        ];
    }

    /**
     * @dataProvider provideTryFromCaseInsensitive
     */
    public function testTryFromCaseInsensitive(string $value, ?Field $expectedResult): void
    {
        self::assertEquals($expectedResult, Field::tryFromCaseInsensitive($value));
    }
}
