<?php

declare(strict_types=1);

namespace Tests\FileImport;

use App\ImportProducts\Enum\BooleanValues;
use App\ImportProducts\Enum\Field;
use App\ImportProducts\ImportProduct;
use PHPUnit\Framework\TestCase;

class ImportProductTest extends TestCase
{
    private function provideData(): array
    {
        return [
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['P0001', 'TV', '32” Tv', '10', '399.99'],
            ],
            [
                [Field::CostInGBP, Field::Discontinued, Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock],
                ['399.99', 'yes', 'P0001', 'TV', '32” Tv', '10'],
            ],
            [
                [Field::ProductDescription, Field::Stock, Field::ProductCode, Field::ProductName, Field::CostInGBP, Field::Discontinued],
                ['32” Tv', '10', 'P0001', 'TV', '399.99', 'no'],
            ],
        ];
    }

    /**
     * @dataProvider provideData
     */
    public function testDifferentHeadersOrdering(array $fieldsMap, array $data): void
    {
        $item = ImportProduct::fromArray($data, $fieldsMap);

        foreach ($fieldsMap as $index => $field) {
            match ($field) {
                Field::ProductCode => self::assertEquals($data[$index], $item->getProductCode()),
                Field::ProductName => self::assertEquals($data[$index], $item->getProductName()),
                Field::ProductDescription => self::assertEquals($data[$index], $item->getProductDescription()),
                Field::Stock => self::assertEquals($data[$index], $item->getStock()),
                Field::CostInGBP => self::assertEquals($data[$index], $item->getCostInGBP()),
                Field::Discontinued => self::assertEquals(BooleanValues::Yes->value === ($data[$index] ?? null), $item->isDiscontinued()),
            };
        }
    }

    private function provideInvalidData(): array
    {
        return [
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['P0001', 'TV', '32” Tv', '10', 'abc'],
            ],
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['P0001', 'TV', '32” Tv', 'abc', '11'],
            ],
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['', 'TV', '32” Tv', '10', '11'],
            ],
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['P0001', '', '32” Tv', '10', '11'],
            ],
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['P0001', 'TV', '', '10', '11'],
            ],
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['P0001', 'TV', '32” Tv', '', '11'],
            ],
            [
                [Field::ProductCode, Field::ProductName, Field::ProductDescription, Field::Stock, Field::CostInGBP, Field::Discontinued],
                ['P0001', 'TV', '32” Tv', '10', ''],
            ],
        ];
    }

    /**
     * @dataProvider provideInvalidData
     */
    public function testInvalidData(array $fieldsMap, array $data): void
    {
        $this->expectException(\InvalidArgumentException::class);
        ImportProduct::fromArray($data, $fieldsMap);
    }
}
