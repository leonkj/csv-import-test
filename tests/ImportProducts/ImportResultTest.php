<?php

declare(strict_types=1);

namespace Tests\FileImport;

use App\ImportProducts\ImportResult;
use App\ImportProducts\Validation\Error;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ImportResultTest extends TestCase
{
    private ImportResult $classUnderTest;

    protected function setUp(): void
    {
        $this->classUnderTest = new ImportResult();
    }

    public function testSuccessful(): void
    {
        self::assertTrue($this->classUnderTest->isSuccessful());
    }

    public function testHasErrors(): void
    {
        $this->classUnderTest->addError($this->getErrorMock('Some error'));

        self::assertFalse($this->classUnderTest->isSuccessful());
    }

    public function testGetErrors(): void
    {
        $error1 = $this->getErrorMock('Some error');
        $error2 = $this->getErrorMock('Some other error');

        $this->classUnderTest->addError($error1);
        $this->classUnderTest->addError($error2);

        self::assertSame([
            $error1,
            $error2,
        ], $this->classUnderTest->getErrors());
    }

    private function getErrorMock(string $message): MockObject|Error
    {
        return $this->createConfiguredMock(Error::class, [
            'getError' => $message
        ]);
    }
}
