<?php

declare(strict_types=1);

namespace Tests\FileImport\Validation;

use App\ImportProducts\ImportProduct;
use App\ImportProducts\Validation\Validator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{
    private Validator $classUnderTest;

    protected function setUp(): void
    {
        $this->classUnderTest = new Validator();
    }

    public function testErrorHeaders(): void
    {
        $headers = [1, 2, 3];

        $this->expectException(\InvalidArgumentException::class);
        $this->classUnderTest->assertHeadersValid($headers);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testValidHeaders(): void
    {
        $headers = [
            'Product Code',
            'Product Name',
            'Product Description',
            'Stock',
            'Cost In GBP',
            'Discontinued',
        ];

        $this->classUnderTest->assertHeadersValid($headers);
    }

    public function testProductDoesntFitRequirements(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->classUnderTest->assertProductFitsRequirements($this->getProductMock(1, 3));
        $this->classUnderTest->assertProductFitsRequirements($this->getProductMock(100, 1001));
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testProductFitsRequirements(): void
    {
        $this->classUnderTest->assertProductFitsRequirements($this->getProductMock(1, 5));
        $this->classUnderTest->assertProductFitsRequirements($this->getProductMock(10, 3));
        $this->classUnderTest->assertProductFitsRequirements($this->getProductMock(10, 1000));
    }

    private function getProductMock(int $stock, float $cost): MockObject|ImportProduct
    {
        return $this->createConfiguredMock(ImportProduct::class, [
            'getCostInGBP' => $cost,
            'getStock' => $stock,
        ]);
    }
}
