<?php

declare(strict_types=1);

namespace Tests\FileImport\Validation;

use App\ImportProducts\Validation\Error;
use PHPUnit\Framework\TestCase;

class ErrorTest extends TestCase
{
    private function provideErrorData(): array
    {
        return [
            ['Some error', null],
            ['Some other error', null],
            ['Some error', 1],
            ['Some other error', 2],
        ];
    }

    /**
     * @dataProvider provideErrorData
     */
    public function testGetters(string $errorMessage, ?int $lineNumber): void
    {
        $error = new Error($errorMessage, $lineNumber);

        self::assertSame($errorMessage, $error->getError());
        self::assertSame($lineNumber, $error->getLineNumber());
    }
}
