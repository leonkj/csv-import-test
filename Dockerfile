ARG PHP_VERSION=8.1

FROM php:${PHP_VERSION}-cli AS csv-import-test-php

RUN apt-get update && apt-get upgrade -y \
    && apt-get install apt-utils -y \
    && apt-get install git zip vim libzip-dev libssl-dev -y

RUN set -eux; \
    docker-php-ext-install -j$(nproc) pdo_mysql; \
    docker-php-ext-enable opcache;

RUN pecl install xdebug-3.1.3 \
    && docker-php-ext-enable xdebug

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN mkdir /home/app
WORKDIR /home/app
