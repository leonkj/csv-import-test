<?php

declare(strict_types=1);

namespace App\Command;

use App\ImportProducts\ImportResult;
use App\Service\ImportProductsCSVParser;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:import-products-csv',
    description: 'Import a file',
    hidden: false,
)]
class ImportProductsCsvCommand extends Command
{
    private const ARGUMENT_PATH = 'path';

    private ImportProductsCSVParser $fileParser;

    public function __construct(ImportProductsCSVParser $fileParser, string $name = null)
    {
        parent::__construct($name);

        $this->fileParser = $fileParser;
    }

    public function configure(): void
    {
        parent::configure();

        $this->addArgument(self::ARGUMENT_PATH, InputArgument::REQUIRED, 'Full path to the file that is being imported');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $path = $this->getPath($input);

        $result = $this->fileParser->process($path);

        $this->writeSuccessMessage($result, $output);

        if (!$result->isSuccessful()) {
            $this->writeErrorMessage($result, $output);

            return self::FAILURE;
        }

        return self::SUCCESS;
    }

    private function getPath(InputInterface $input): string
    {
        return $input->getArgument(self::ARGUMENT_PATH);
    }

    private function writeSuccessMessage(ImportResult $importResult, OutputInterface $output): void
    {
        if (count($importResult->getProducts()) > 0) {
            $output->writeln('<info>' . count($importResult->getProducts()) . ' items were imported</info>');
        }
    }

    private function writeErrorMessage(ImportResult $importResult, OutputInterface $output): void
    {
        if ($importResult->getUnsuccessfulAmount() > 0) {
            $output->writeln('<error>' . $importResult->getUnsuccessfulAmount() . ' items were not imported</error>');
        }

        foreach ($importResult->getErrors() as $error) {
            $message = '';

            if ($error->getLineNumber()) {
                $message .= 'Line ' . $error->getLineNumber() . ': ';
            }

            $message .= $error->getError();

            $output->writeln('<error> ' . $message . '</error>');
        }
    }
}
