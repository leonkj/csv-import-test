<?php

declare(strict_types=1);

namespace App\ImportProducts;

use App\ImportProducts\Enum\BooleanValues;
use App\ImportProducts\Enum\Field;
use Webmozart\Assert\Assert;
use Webmozart\Assert\InvalidArgumentException;

class ImportProduct
{
    private string $productCode;
    private string $productName;
    private string $productDescription;
    private int $stock;
    private float $costInGBP;
    private bool $discontinued;

    private function __construct()
    {
    }

    public function getProductCode(): string
    {
        return $this->productCode;
    }

    public function getProductName(): string
    {
        return $this->productName;
    }

    public function getProductDescription(): string
    {
        return $this->productDescription;
    }

    public function getStock(): int
    {
        return $this->stock;
    }

    public function getCostInGBP(): float
    {
        return $this->costInGBP;
    }

    public function isDiscontinued(): bool
    {
        return $this->discontinued;
    }

    /**
     * @param string[] $data
     * @param Field[] $fieldsOrdering
     *
     * @return self
     */
    public static function fromArray(array $data, array $fieldsOrdering): self
    {
        $item = new self();
        $errors = [];

        foreach ($fieldsOrdering as $index => $field) {
            $value = trim($data[$index] ?? '');

            try {
                self::assertValid($field, $value);
                $item->setValue($field, $value);
            } catch (InvalidArgumentException $exception) {
                $errors[] = $exception->getMessage();
            }
        }

        if (count($errors) !== 0) {
            throw new \InvalidArgumentException(implode(', ', $errors));
        }

        return $item;
    }

    private static function assertValid(Field $field, string $value): void
    {
        match ($field) {
            Field::ProductCode, Field::ProductName, Field::ProductDescription =>
                Assert::stringNotEmpty($value, sprintf('Field \'%s\' should not be empty', $field->value)),
            Field::Stock => Assert::numeric($value, sprintf('Field \'%s\' should be an integer', $field->value)),
            Field::CostInGBP => Assert::numeric($value, sprintf('Field \'%s\' should be numeric', $field->value)),
            Field::Discontinued => self::assertBoolean($value, sprintf(
                'Invalid value provided for field \'%s\'. Valid values are: %s.',
                $field->value,
                implode(', ', self::getBooleanValues())
            )),
        };
    }

    private static function assertBoolean(string $value, string $message): void
    {
        if ('' !== $value && !in_array(mb_strtolower($value), self::getBooleanValues(), true)) {
            throw new InvalidArgumentException($message);
        }
    }

    /**
     * @return string[]
     */
    private static function getBooleanValues(): array
    {
        return array_column(BooleanValues::cases(), 'value');
    }

    private function setValue(Field $field, ?string $value): void
    {
        match ($field) {
            Field::ProductCode => $this->productCode = $value,
            Field::ProductName => $this->productName = $value,
            Field::ProductDescription => $this->productDescription = $value,
            Field::Stock => $this->stock = (int)$value,
            Field::CostInGBP => $this->costInGBP = (float)$value,
            Field::Discontinued => $this->discontinued = BooleanValues::Yes->value === mb_strtolower($value),
        };
    }
}
