<?php

declare(strict_types=1);

namespace App\ImportProducts;

class CSVFileReader
{
    public function read(string $path): \Generator
    {
        $file = fopen($path, 'rb');

        while (false !== ($lineData = fgetcsv($file, 1000, ","))) {
            yield $lineData;
        }

        fclose($file);
    }
}
