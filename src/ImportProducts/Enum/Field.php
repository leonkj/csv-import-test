<?php

declare(strict_types=1);

namespace App\ImportProducts\Enum;

enum Field: string
{
    case ProductCode = 'Product Code';
    case ProductName = 'Product Name';
    case ProductDescription = 'Product Description';
    case Stock = 'Stock';
    case CostInGBP = 'Cost In GBP';
    case Discontinued = 'Discontinued';

    public static function tryFromCaseInsensitive(string $value): ?self
    {
        foreach (self::cases() as $case) {
            if (mb_strtolower($case->value) === mb_strtolower($value)) {
                return $case;
            }
        }

        return null;
    }
}
