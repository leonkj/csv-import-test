<?php

declare(strict_types=1);

namespace App\ImportProducts\Enum;

enum BooleanValues: string
{
    case Yes = 'yes';
    case No = 'no';
}
