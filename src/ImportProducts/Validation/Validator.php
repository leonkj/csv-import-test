<?php

declare(strict_types=1);

namespace App\ImportProducts\Validation;

use App\ImportProducts\Enum\Field;
use App\ImportProducts\ImportProduct;

class Validator
{
    private const MIN_PRICE = 5;
    private const MIN_STOCK = 10;
    private const MAX_PRICE = 1000;

    /**
     * @param string[] $headers
     */
    public function assertHeadersValid(array $headers): void
    {
        $providedHeaders = array_map(static fn(string $header) => mb_strtolower($header), $headers);

        $missingHeaders = [];
        foreach (array_column(Field::cases(), 'value') as $header) {
            if (!in_array(mb_strtolower($header), $providedHeaders, true)) {
                $missingHeaders[] = $header;
            }
        }

        if (count($missingHeaders) > 0) {
            throw new \InvalidArgumentException(
                'Provided file doesn\'t contain required headers. Missing headers are: ' . implode(', ', $missingHeaders)
            );
        }
    }

    public function assertProductFitsRequirements(ImportProduct $product): void
    {
        if ($product->getCostInGBP() < self::MIN_PRICE && $product->getStock() < self::MIN_STOCK) {
            throw new \InvalidArgumentException(sprintf(
                'Item which costs less that %d and has less than %d stock is not imported',
                self::MIN_PRICE,
                self::MIN_STOCK
            ));
        }

        if ($product->getCostInGBP() > self::MAX_PRICE) {
            throw new \InvalidArgumentException(sprintf(
                'Item which costs over %d is not imported',
                self::MAX_PRICE
            ));
        }
    }
}
