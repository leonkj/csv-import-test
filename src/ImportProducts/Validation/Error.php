<?php

declare(strict_types=1);

namespace App\ImportProducts\Validation;

class Error
{
    public function __construct(
        private string $error,
        private ?int $lineNumber = null,
    )
    {
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function getLineNumber(): ?int
    {
        return $this->lineNumber;
    }
}
