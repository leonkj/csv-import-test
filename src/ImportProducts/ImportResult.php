<?php

declare(strict_types=1);

namespace App\ImportProducts;

use App\ImportProducts\Validation\Error;

class ImportResult
{
    /**
     * @var Error[]
     */
    private array $errors = [];

    /**
     * @var ImportProduct[]
     */
    private array $products = [];

    private int $unsuccessfulAmount = 0;

    public function addError(Error $error): void
    {
        $this->errors[] = $error;
    }

    public function isSuccessful(): bool
    {
        return 0 === count($this->errors);
    }

    /**
     * @return Error[]
     */
    public function getErrors(): iterable
    {
        return $this->errors;
    }

    public function addProduct(ImportProduct $product): void
    {
        $this->products[] = $product;
    }

    /**
     * @return ImportProduct[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    public function getUnsuccessfulAmount(): int
    {
        return $this->unsuccessfulAmount;
    }

    public function incrementUnsuccessful(): void
    {
        $this->unsuccessfulAmount++;
    }
}
