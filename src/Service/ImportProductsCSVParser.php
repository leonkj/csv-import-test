<?php

declare(strict_types=1);

namespace App\Service;

use App\ImportProducts\CSVFileReader;
use App\ImportProducts\Enum\Field;
use App\ImportProducts\ImportProduct;
use App\ImportProducts\ImportResult;
use App\ImportProducts\Validation\Error;
use App\ImportProducts\Validation\Validator;
use Symfony\Component\Filesystem\Filesystem;

class ImportProductsCSVParser
{
    public const HEADERS_LINE_NUMBER = 1;

    private Filesystem $filesystem;
    private Validator $validator;
    private CSVFileReader $fileReader;

    public function __construct(Filesystem $filesystem, Validator $validator, CSVFileReader $fileReader)
    {
        $this->filesystem = $filesystem;
        $this->validator = $validator;
        $this->fileReader = $fileReader;
    }

    public function process(string $filePath): ImportResult
    {
        $result = new ImportResult();

        if (!$this->filesystem->exists($filePath)) {
            $result->addError(new Error(sprintf('File %s does not exist', $filePath)));

            return $result;
        }

        $lineNumber = 0;
        $fieldsOrdering = [];
        foreach ($this->fileReader->read($filePath) as $lineData) {
            $lineNumber++;

            if ($this->isHeaderLine($lineNumber)) {
                try {
                    $this->validator->assertHeadersValid($lineData);
                } catch (\InvalidArgumentException $exception) {
                    $result->addError(new Error($exception->getMessage()));

                    return $result;
                }

                $fieldsOrdering = $this->getFieldsOrdering($lineData);

                continue;
            }

            try {
                $product = ImportProduct::fromArray($lineData, $fieldsOrdering);
                $this->validator->assertProductFitsRequirements($product);
                $result->addProduct($product);
            } catch (\InvalidArgumentException $exception) {
                $result->addError(new Error($exception->getMessage(), $lineNumber));
                $result->incrementUnsuccessful();
            }
        }

        return $result;
    }

    private function isHeaderLine(int $lineNumber): bool
    {
        return self::HEADERS_LINE_NUMBER === $lineNumber;
    }

    /**
     * @param string[] $fileHeaders
     *
     * @return Field[]
     */
    private function getFieldsOrdering(array $fileHeaders): array
    {
        $fieldsOrdering = [];
        foreach ($fileHeaders as $index => $field) {
            $fieldsOrdering[$index] = Field::tryFromCaseInsensitive($field);
        }

        return $fieldsOrdering;
    }
}
